# Introduction

Useful Git pre-commit hooks when using Makefiles such as `black`, `pylint`, `bandit`, `radon`, `pytests`

It uses Yelp's pre-commit framework for adding git hooks locally.

Inspired by https://github.com/smian/pre-commit-makefile

## Dependencies

* [`bash`](https://www.gnu.org/software/bash/bash.html)
* [`git`](https://github.com/git/git)

## Usage

### Step 1: Install the pre-commit package (MAC OS)

Using pip:

```shell
pip install pre-commit
```

Using [brew](https://brew.sh/):

```shell
brew install pre-commit
```

Using [conda](https://conda.io/) (via [conda-forge](https://conda-forge.org/)):

```shell
conda install -c conda-forge pre_commit
```

Using [poetry](https://poetry.eustace.io/):

```shell
poetry add pre-commit --dev
```

### Step 2: Add the following to `.pre-commit-config.yaml`

```shell
cat <<EOF > .pre-commit-config.yaml
- repo: git@gitlab.com:coco-codez/pre-commit-python-build-makefile.git
  rev: master
  hooks:
    - id: makefile-precommit
EOF
```

### Step 3: Install the git pre-commit hook to your local project

```shell
pre-commit install
```

If you want to run the checks on-demand (outside of git hooks), run after you when through the setup below:

```shell
pre-commit run --all-files --verbose
```

### Step 4: Create a Makefile and define a `make precommit` target

Define `make precommit` target that executes any tests, formatters that you would like. You can create you own Makefile or use the starter [Makefile](examples/Makefile) under`examples/Makefile`.

## License

The code in this repo is licensed under the [MIT License](LICENSE).
